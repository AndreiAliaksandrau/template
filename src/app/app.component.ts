import { TemplateRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Phone } from './phone';
import { Component, OnInit } from '@angular/core';
import { NgModel, NgForm, FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { User } from "./user";
import { HttpService } from "./http.service";
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserService]
})

export class AppComponent implements OnInit {
  @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;

  editedUser: User;
  users: Array<User>;
  isNewRecord: boolean;
  statusMessage: string;

  constructor(private userService: UserService) {
    this.users = new Array<User>();
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  private loadUsers() {
    this.userService.getUsers().subscribe((data: User[]) => {
      this.users = data;
    });
  }

  addUser() {
    this.editedUser = new User(0, "", 0);
    this.users.push(this.editedUser);
    this.isNewRecord = true;
  }

  editUser(user: User) {
    this.editedUser = new User(user.id, user.name, user.age);
  }

  loadTemplate(user: User) {
    if(this.editedUser && this.editedUser.id == user.id) {
      return this.editTemplate;
    } else {
      return this.readOnlyTemplate;
    }
  }

  saveUser(user: User) {
    if (this.isNewRecord) {
      this.userService.createUser(this.editedUser).subscribe(data => {
        this.statusMessage = "Data has been successfully added";
        this.loadUsers();
      });
      this.isNewRecord = false;
      this.editedUser = null;
    } else {
      this.userService.updateUser(this.editedUser.id, this.editedUser).subscribe(data => {
        this.statusMessage = "Data has been successfully updated";
        this.loadUsers();
      });
      this.editedUser = null;
    }
  }

  cancel() {
    if (this.isNewRecord) {
      this.users.pop();
      this.isNewRecord = false;
    }
    this.editedUser = null;
  }

  deleteUser(user: User) {
    this.userService.deleteUser(user.id).subscribe(data => {
      this.statusMessage = "Data has been successfully deleted";
      this.loadUsers();
    });
  }
}