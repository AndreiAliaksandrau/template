import { Observable } from 'rxjs/Observable';
import { ComponentCanDeactivate } from './exit.about.guard';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'about-app',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, ComponentCanDeactivate {
  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
      return confirm("Are u sure to leave page?")
    } else {
      return true;
    }
  }

  saved: boolean = false;
  save() {
    this.saved = true;
  }

  constructor() { }

  ngOnInit() {
  }

}
