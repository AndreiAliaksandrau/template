import { FactorialPipe } from './factorial.pipe';
import { ExitAboutGuard } from './about/exit.about.guard';
import { DataService } from './data.service';
import { BoldDirective } from './bold.directive';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { ChildComponent } from './child/child.component';
import { LogService } from './log.service';
import { HttpClientModule } from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ItemComponent } from './item/item.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { ItemStatComponent } from './item-stat/item-stat.component';
import { AboutGuard } from "./about/about.guard";
import { JoinPipe } from './join.pipe';
import { UserService } from './user.service';

const itemRoutes: Routes = [
  { path: 'details', component: ItemDetailsComponent },
  { path: 'stat', component: ItemStatComponent }
];

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent, canActivate: [AboutGuard], canDeactivate: [ExitAboutGuard] },
  { path: 'item/:id', component: ItemComponent },
  { path: 'item/:id', component: ItemComponent, children: itemRoutes },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    BoldDirective,
    HomeComponent,
    AboutComponent,
    NotFoundComponent,
    ItemComponent,
    ItemDetailsComponent,
    ItemStatComponent,
    FactorialPipe,
    JoinPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService, LogService, AboutGuard, ExitAboutGuard, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }