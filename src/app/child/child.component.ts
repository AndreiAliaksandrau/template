import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { LogService } from '../log.service';
import { Phone } from '../phone';

@Component({
  selector: 'data-app',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
  items: Phone[] = [];
  constructor(private dataService: DataService) { }

  ngOnInit() {
  }
}
