import { Injectable } from "@angular/core";
import { Phone } from './phone';
import { LogService } from "./log.service";

@Injectable()
export class DataService {
  constructor(private logService: LogService) { }
}