import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { User } from "./user";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HttpService {
    constructor(private http: HttpClient) { }

    getFactorial(num: number) {
        const params = new HttpParams().set('number', num.toString());
        return this.http.get('http://localhost:5000/api/values/GetFactorial?number=', { params });
    }

    getUsers() {
        return this.http.get('../assets/user.json');
    }
}