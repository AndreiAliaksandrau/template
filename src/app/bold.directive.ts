import { Directive, ElementRef, Renderer2, HostListener, HostBinding, Input, OnInit } from "@angular/core";

@Directive({
    selector: '[bold]'
})
export class BoldDirective implements OnInit {
    @Input('bold') selectedSize = '18px';
    @Input() defaultSize = '16px';

    private fontSize: string;
    private fontWeight = 'normal';

    constructor() { }

    ngOnInit(): void {
        this.fontSize = this.defaultSize;
    }

    @HostBinding('style.fontSize') get getFontFize() {
        return this.fontSize;
    }

    @HostBinding('style.fontWeight') get getFontWeight() {
        return this.fontWeight;
    }

    @HostListener('mouseenter') onMouseEnter() {
        this.fontWeight = 'bold';
        this.fontSize = this.selectedSize;
    }

    @HostListener('mouseleave') onMouseLeave() {
        this.fontWeight = 'normal';
        this.fontSize = this.defaultSize;
    }
}